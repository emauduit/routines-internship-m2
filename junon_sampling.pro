;+
;
; : Description:
; 
; Split a data file (containing a time-frequency dynamical spectrum) in frequency in bands of a chosen width then find the number of
; corresponding channels which is the most suitable and divide the file according to this number, ni. Some channels may be left aside, those 
; losses will be evaluated.
; Each of these frequency bands will be divided in squared panels of sizes ni*ni (we want to keep the time resolution). Those panels will then
; be integrated in frequency overt two different integers ni1 and ni2 (7 and 17 here) in order to cover all the drifts we want to study. those ; integers are multiples of ni.
; Then each ni*ni/ni1 panel is divide in smaller ones of sizes (ni/ni1)*(ni/ni1). Traitement_junon.pro will then be applied to each one of 
; them and retrieve useful parameters for the study.
; At the begining, an 3D array is created, for each panel (ni/ni1)*(ni/ni1) it will contain all the informations about it.
; 
; :Uses:
;
; read_junon_data
; traitement_junon
; prep_ponderation
; plot_data
; junolib
; 
; :Parameters:
; 
; INPUT:
; 
;   folder : path of the folder containing the files to process
;   nbandes : number of bands in frequency in which you want to split the spectra
;   dfrecouv : range over which wewant the bands to overlap in MHz
;   ni1,ni2 : integrations factor on which we want to integrate in frequency, they have to be multiple of ni and 5. Here 7 et 17
;   nfstart : the index of the file you wanna start with (in case the run crashes)
;
; OUTPUT :
;
; A saveset containing the list of the read files, their size and the time it took to process them.
; And for each file, a saveset containing two arrays, one for each polarization of dimension (nbandes*(ncarres*ni1)*15) containing  all the 
; parameters about each panel.
;
;-
;

pro junon_sampling, folder, nbandes,dfrecouv,ni1, ni2, nfstart

  files = file_Search(folder+'/*extract2.dat',count=nfiles)
  restore, '/home/emauduit/lecture/tag2.sav'
  times=fltarr(nfiles)
  sizes=fltarr(nfiles)
  
  
  for nf=nfstart,nfiles-1 do begin

     tic, /profiler
     clock=tic()
     rien = JunoOpen(files[nf], desc)
     JunoVerb, files[nf], desc, 0
     stat=fstat(desc.lun)
     close, desc.lun
     free_lun, desc.lun
     sizes(nf)=((stat.size/1024.)/1024.)/1024. ; Size of thefile in Gb
     names=files[nf].split('/')
     path_file=files[nf]
     nf_plot=0  ;number of files containing plots
     nplot=0 ;number of plots for the current file
     
     ;if the file is too big we can't read it in one time, then we read file 20 Go by 20 Go

     if (sizes(nf) le 20.) then begin

        psnames=[names(-1).substring(0,-5)+'_'+strtrim(nf_plot,2)]

        set_ps,psnames(0)+'.ps', /landscape
        device, /color
        !p.multi=[0,3,3,0,0]
      
        read_junon_data, files[nf], LH, RH, t,f,fmin=8., fmax=41.

        dim=size(LH)
        n=dim(1)                ; nombre de colonnes (temps)
        p=dim(2)                ; nombre de lignes (frequence)


        header=junoHeader(files[nf])
        fmin=header.freq[0]
        fmax=header.freq[header.nfreq-1]

        if (fmin lt 8. ) then fmin=8.
        if (fmax gt 41.) then fmax=41.

        lfreq=fmax-fmin ;frequency width of the dynamical spectra
        Nc_tot=floor(lfreq*1e3/dfkhz) ; corresponding number of channels

        nbandes=ceil(Nc_tot/2975.)
        if (nbandes gt 1) then dfrecouv=((nbandes*2975L)-Nc_tot)*dfkhz*1e-3/(nbandes-1) else dfrecouv=0.

       ; Calculation of the most adapted number of channels for the bands

        dfreq=(lfreq+((nbandes-1)*dfrecouv))/nbandes ; frequency width of the bands in MHz
        ni=round(dfreq*1e3/dfkhz)                 ; number of channels in the band, dfkhz is the frequency resolution of the instrument in kHZ
        while ((ni mod (ni1*ni2*5)) ne 0) do ni=ni-1
        dfreq=dfkhz*ni                    ; calculation of the real frequency width of the bands in kHz
        nrecouv=round(dfrecouv*1.e3/dfkhz) ; number of channels for the overlap


       ;evaluation of the number of channels in time and frequency to be removed


        perte_freq=(Nc_tot+nrecouv*(nbandes-1)+1) mod ni ; number of frequency channels lost
        perte_time=n mod ni                              ; number of time channels lost
        ncarres=n/ni

        p_lf=perte_freq/2
        p_hf=perte_freq/2
        p_lt=perte_time/2
        p_ht=perte_time/2
        if ((perte_freq mod 2) ne 0) then p_hf=p_hf+1
        if ((perte_time mod 2) ne 0) then p_ht=p_ht+1


        LH=LH(p_lt :n-p_ht-1,p_lf :p-p_hf-1)
        RH=RH(p_lt :n-p_ht-1,p_lf :p-p_hf-1)

       ; preparation of the radon transform

        rho=[0.] & nt=ceil(!pi*ni/(5*ni1))
        theta=findgen(nt)*180./nt
        yc1=prep_ponderation(ni/(5*ni1),ni/(5*ni1))

        wt=where(((theta ge 15.) and (theta le 75.)) or ((theta ge 105.) and (theta le 165.)),nrd)
        theta_rd=findgen(nrd)*120./nrd

       ; preparation of the array containing the parameters of interest

        params_LH=fltarr(nbandes,ncarres*ni1,15)-450.
        params_RH=fltarr(nbandes,ncarres*ni1,15)-450.



        for i=0,(nbandes-1) do begin

           if i ge 1 then nr=nrecouv else nr=0 ;prend en compte le recouvrement dans le découpage
           for j=0,(ncarres-1) do begin

              LH1=LH((j*ni):((j+1)*ni)-1,i*(ni-1-nr):(i+1)*(ni-1)-i*nr)
              RH1=RH((j*ni):((j+1)*ni)-1,i*(ni-1-nr):(i+1)*(ni-1)-i*nr)

           ;integration over ni1 (and eventually ni2) for each panel (ni*ni)

              LH1=rebin(LH1,ni,ni/ni1)
              RH1=rebin(RH1,ni,ni/ni1)

              for k1=0,ni1-1 do begin

                 traitement_junon,LH1[k1*ni/ni1:(k1+1)*ni/ni1-1,*],yc1,xlh1,tflh1,rd_lh1
                 traitement_junon,RH1[k1*ni/ni1:(k1+1)*ni/ni1-1,*],yc1,xrh1,tfrh1,rd_rh1

                 rd_lh1=smooth(rd_lh1(wt),4, /edge_wrap)
                 rd_rh1=smooth(rd_rh1(wt), 4, /edge_wrap)

                 w_lh=where(rd_LH1 eq max(rd_LH1))
                 w_rh=where(rd_RH1 eq max(rd_RH1))

                 tag_lh=0
                 tag_rh=0

             ; adjusting the bounds of the gaussian fit whether they are higher or lower than 0 or 120 (it is done over +45° and -45° around the position of the maximum), if it is not between the bounds the portion that is out is reported at the other extremity

                 if (w_lh-nrd/3 ge 0) then begin
                    if ((w_lh+nrd/3-1 le nrd-1)) then begin
                       fit_lh1=gaussfit(theta_rd[w_lh-nrd/3:w_lh+nrd/3-1],rd_LH1[w_lh-nrd/3:w_lh+nrd/3-1], coeff_lh,chisq=chi2_lh, nterms=4, sigma=err_par_lh, yerror=err_rd_lh)
                       snr_lh1=snr_func(rd_LH1[w_lh-nrd/3:w_lh+nrd/3-1],theta_rd[w_lh-nrd/3:w_lh+nrd/3-1],coeff_lh)
                       theta_lh=theta_rd[w_lh-nrd/3:w_lh+nrd/3-1]
                    endif else begin
                       gap_ind=w_lh+nrd/3-(nrd-1)
                       fit_lh1=gaussfit(theta_rd[w_lh-nrd/3-gap_ind :nrd-1],rd_LH1[w_lh-nrd/3-gap_ind :nrd-1], coeff_lh, chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                       snr_lh1=snr_func(rd_LH1[w_lh-nrd/3-gap_ind :nrd-1],theta_rd[w_lh-nrd/3-gap_ind :nrd-1],coeff_lh)
                       theta_lh=theta_rd[w_lh-nrd/3-gap_ind :nrd-1]
                    endelse
                    
                 endif else begin
                    
                    gap_ind=nrd/3-w_lh
                    fit_lh1=gaussfit(theta_rd[0:w_lh+nrd/3-1+gap_ind],rd_LH1[0:w_lh+nrd/3-1+gap_ind], coeff_lh, chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                    snr_lh1=snr_func(rd_LH1[0:w_lh+nrd/3-1+gap_ind],theta_rd[0:w_lh+nrd/3-1+gap_ind],coeff_lh)
                    theta_lh=theta_rd[0:w_lh+nrd/3-1+gap_ind]
                 endelse
                   
                 if (w_rh-nrd/3 ge 0) then begin
                    if ((w_rh+nrd/3-1 le nrd-1)) then begin
                       fit_rh1=gaussfit(theta_rd[w_rh-nrd/3:w_rh+nrd/3-1],rd_RH1[w_rh-nrd/3:w_rh+nrd/3-1], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                       snr_rh1=snr_func(rd_rH1[w_rh-nrd/3:w_rh+nrd/3-1],theta_rd[w_rh-nrd/3:w_rh+nrd/3-1],coeff_rh)
                       theta_rh=theta_rd[w_rh-nrd/3:w_rh+nrd/3-1]
                    endif else begin
                       gap_ind=w_rh+nrd/3-(nrd-1)
                       fit_rh1=gaussfit(theta_rd[w_rh-nrd/3-gap_ind :nrd-1],rd_LH1[w_rh-nrd/3-gap_ind :nrd-1], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                       snr_rh1=snr_func(rd_RH1[w_rh-nrd/3-gap_ind :nrd-1],theta_rd[w_rh-nrd/3-gap_ind :nrd-1],coeff_rh)
                       theta_rh=theta_rd[w_rh-nrd/3-gap_ind :nrd-1]
                    endelse
                 endif else begin
                    gap_ind=nrd/3-w_rh
                    fit_rh1=gaussfit(theta_rd[0:w_rh+nrd/3-1+gap_ind],rd_RH1[0:w_rh+nrd/3-1+gap_ind], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                    snr_rh1=snr_func(rd_rH1[0:w_rh+nrd/3-1+gap_ind],theta_rd[0:w_rh+nrd/3-1+gap_ind],coeff_rh)
                    theta_rh=theta_rd[0:w_rh+nrd/3-1+gap_ind]
                 endelse

               ; computing the real value corresponding to the position of the maximum, because of the reduction of the interval for the gaussian adjsutement

                 if (coeff_lh(1) lt 60.) then coeff_lh(1)=coeff_lh(1)+15. else coeff_lh(1)=coeff_lh(1)+45.
                 if (coeff_rh(1) lt 60.) then coeff_rh(1)=coeff_rh(1)+15. else coeff_rh(1)=coeff_rh(1)+45.

                 tmin=t((j*ni+k1*ni/ni1)) & tmax=t(j*ni+(k1+1)*ni/ni1-1)
                 fmin=f(i*(ni-1-nr)) & fmax=f((i+1)*(ni-1)-i*nr)

               ;calculation of the drift

                 slope_lh=tan(coeff_lh(1)*!dtor)*dfkhz*ni1/dtmsec ; pente en MHz/s
                 slope_rh=tan(coeff_rh(1)*!dtor)*dfkhz*ni1/dtmsec
                 
                 ;ploting the dynamical spectra, the 2D-FFT and the radon transform if SNR >3, not exceeding 3000 lines of plots in the file

                 if ((snr_lh1 ge 3.) or (snr_rh1 ge 3.)) then begin
                    nplot=nplot+1
                      
                    tag_rh=1 & tag_lh=1
                    plot_data, xlh1, tflh1, rd_lh1, theta_rd, 'LH', slope_lh, snr_lh1, chi2_lh, coeff_lh(2), tmin, tmax, fmin, fmax
                    oplot, theta_lh, fit_lh1, line=2, color=250.
                    oplot,[0,120], [max(rd_lh1), max(rd_lh1)], line=2, color=250.

                    plot_data, xrh1, tfrh1, rd_rh1, theta_rd, 'RH', slope_rh, snr_rh1,chi2_rh, coeff_rh(2), tmin, tmax, fmin, fmax
                    oplot, theta_rh, fit_rh1, line=2, color=250.
                    oplot,[0,120], [max(rd_rh1), max(rd_rh1)], line=2, color=250.
                 endif
                 if (nplot gt 3000) then begin
                    device, /close
                    exit_ps
                    ps_pdf, psnames(-1)+'.ps',/remove
                    spawn,'cp '+psnames(-1)+'.pdf ' +'/home/emauduit/traitement_massif/results/'+ names(-3)+'/plot/'+ names(-2)+'/'+psnames(-1)+'.pdf'
                    spawn,'rm '+psnames(-1)+'.pdf '
                   
                    nf_plot=nf_plot+1
                    psnames=[psnames,names(-1).substring(0,-5)+'_'+strtrim(nf_plot,2)]
                    set_ps,psnames(-1)+'.ps', /landscape
                    !p.multi=[0,3,3,0,0]
                    nplot=0
                 endif
                 

                 params_LH[i,j*ni1+k1,*]=[nf,tag_lh,coeff_lh(0),err_par_lh(0), coeff_lh(1),err_par_lh(1) ,coeff_lh(2),err_par_lh(2), chi2_lh, snr_lh1, err_rd_lh,tmin, tmax, fmin, fmax]
                 params_RH[i,j*ni1+k1,*]=[nf,tag_rh,coeff_rh(0),err_par_rh(0), coeff_rh(1),err_par_rh(1) ,coeff_rh(2),err_par_rh(2), chi2_rh, snr_rh1,err_rd_rh,tmin, tmax, fmin, fmax]


              endfor

           endfor

        endfor

        LH=0 & RH=0
        save, path_file , params_LH, params_RH, file='/home/emauduit/traitement_massif/results/'+ names(-3)+'/parameters/'+ names(-2)+'/parameters_'+names(-1).substring(0,-5)+'.sav' 
       
     endif else begin

      ; computing the number of subfiles the file has to be split in

        n_open=floor(sizes(nf)/20.)
        r_open=sizes(nf) mod 20.
       
        yy=fix(strmid(files[nf],27,4,/rev))
        mo=fix(strmid(files[nf],23,2,/rev))
        dd=fix(strmid(files[nf],21,2,/rev))
        jd0=julday(mo,dd,yy,0.,0.,0.)

        tini=(desc.jd1-jd0)*24.d0
        dt_coupure=(((desc.jd2-jd0)*24.d0 - (desc.jd1-jd0)*24.d0)/sizes(nf))*20.
        dt_recouv=0.

        psnames=[]


        for nc=0,n_open-1 do begin
           nf_plot=0

           psnames=[psnames,names(-1).substring(0,-5)+'_'+strtrim(nc,2)+'_'+strtrim(nf_plot,2)]

           set_ps,psnames(-1)+'.ps', /landscape
           device, /color
           !p.multi=[0,3,3,0,0]
          
           read_junon_data, files[nf], LH, RH, t,f,tmin=tini+nc*dt_coupure-dt_recouv,tmax=tini+(nc+1)*dt_coupure,fmin=8., fmax=41. 
           dim=size(LH)
           n=dim(1)             ; number of columns (time)
           p=dim(2)             ; number of lines (frequency)


           header=junoHeader(files[nf])
           fmin=header.freq[0]
           fmax=header.freq[header.nfreq-1]

           if (fmin lt 8. ) then fmin=8.
           if (fmax gt 41.) then fmax=41.

           lfreq=fmax-fmin
           Nc_tot=floor(lfreq*1e3/dfkhz)

           nbandes=ceil(Nc_tot/2975.)
           if (nbandes gt 1) then dfrecouv=((nbandes*2975L)-Nc_tot)*dfkhz*1e-3/(nbandes-1) else dfrecouv=0.

       ; 

           dfreq=(lfreq+((nbandes-1)*dfrecouv))/nbandes ; width in frequency of the bands (MHZ)
           ni=round(dfreq*1e3/dfkhz)                    ; number of channels in the band , dfkhz is the frequency resolution in kHZ
           while ((ni mod (ni1*ni2*5)) ne 0) do ni=ni-1
           dfreq=dfkhz*ni                    ; computing the true width of the band kHz
           nrecouv=round(dfrecouv*1.e3/dfkhz) ; numbers of channels for the overlap between the bands


       ;computing the number of channels in time and frequency to be removed


           perte_freq=(Nc_tot+nrecouv*(nbandes-1)+1) mod ni ; frequency channels loss
           perte_time=n mod ni                              ; time channels loss
           dt_recouv=(perte_time+1)*dtmsec
           ncarres=n/ni

           p_lf=perte_freq/2
           p_hf=perte_freq/2
           p_lt=perte_time/2
           p_ht=perte_time/2
           if ((perte_freq mod 2) ne 0) then p_hf=p_hf+1
           if ((perte_time mod 2) ne 0) then p_ht=p_ht+1


           LH=LH(0:n-perte_time-1,p_lf :p-p_hf-1)
           RH=RH(0:n-perte_time-1,p_lf :p-p_hf-1)

       ; preparation of the radon transform

           rho=[0.] & nt=ceil(!pi*ni/(5*ni1))
           theta=findgen(nt)*180./nt
           yc1=prep_ponderation(ni/(5*ni1),ni/(5*ni1))
           wt=where(((theta ge 15.) and (theta le 75.)) or ((theta ge 105.) and (theta le 165.)),nrd)
           theta_rd=findgen(nrd)*120./nrd

       ; preparation of the array containing the parameters of interest

           params_LH=fltarr(nbandes,ncarres*ni1,15)-450.
           params_RH=fltarr(nbandes,ncarres*ni1,15)-450.



           for i=0,(nbandes-1) do begin

              if i ge 1 then nr=nrecouv else nr=0 ;takes the overlap into account for the splitting

              for j=0,(ncarres-1) do begin

                 LH1=LH((j*ni):((j+1)*ni)-1,i*(ni-1-nr):(i+1)*(ni-1)-i*nr)
                 RH1=RH((j*ni):((j+1)*ni)-1,i*(ni-1-nr):(i+1)*(ni-1)-i*nr)

           ;integrating each band over ni1 or ni2 in frequency
                 LH1=rebin(LH1,ni,ni/ni1)
                 RH1=rebin(RH1,ni,ni/ni1)

                 for k1=0,ni1-1 do begin

                    traitement_junon,LH1[k1*ni/ni1:(k1+1)*ni/ni1-1,*],yc1,xlh1,tflh1,rd_lh1
                    traitement_junon,RH1[k1*ni/ni1:(k1+1)*ni/ni1-1,*],yc1,xrh1,tfrh1,rd_rh1

                    rd_lh1=smooth(rd_lh1(wt),4, /edge_wrap)
                    rd_rh1=smooth(rd_rh1(wt), 4, /edge_wrap)

                    w_lh=where(rd_LH1 eq max(rd_LH1))
                    w_rh=where(rd_RH1 eq max(rd_RH1))

                    tag_lh=0
                    tag_rh=0

             ; adjusting the bounds of the gaussian fit whether they are higher or lower than 0 or 120 (it is done over +45° and -45° around the position of the maximum), if it is not between the bounds the portion that is out is reported at the other extremity

                    if (w_lh-nrd/3 ge 0) then begin
                       if ((w_lh+nrd/3-1 le nrd-1)) then begin
                          fit_lh1=gaussfit(theta_rd[w_lh-nrd/3:w_lh+nrd/3-1],rd_LH1[w_lh-nrd/3:w_lh+nrd/3-1], coeff_lh,chisq=chi2_lh, nterms=4,sigma=err_par_lh,yerror=err_rd_lh)
                          snr_lh1=snr_func(rd_LH1[w_lh-nrd/3:w_lh+nrd/3-1],theta_rd[w_lh-nrd/3:w_lh+nrd/3-1],coeff_lh)
                          theta_lh=theta_rd[w_lh-nrd/3:w_lh+nrd/3-1]
                       endif else begin
                          gap_ind=w_lh+nrd/3-(nrd-1)
                          fit_lh1=gaussfit(theta_rd[w_lh-nrd/3-gap_ind :nrd-1],rd_LH1[w_lh-nrd/3-gap_ind :nrd-1], coeff_lh, chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                          snr_lh1=snr_func(rd_LH1[w_lh-nrd/3-gap_ind :nrd-1],theta_rd[w_lh-nrd/3-gap_ind :nrd-1],coeff_lh)
                          theta_lh=theta_rd[w_lh-nrd/3-gap_ind :nrd-1]
                       endelse
                    
                    endif else begin
                    
                       gap_ind=nrd/3-w_lh
                       fit_lh1=gaussfit(theta_rd[0:w_lh+nrd/3-1+gap_ind],rd_LH1[0:w_lh+nrd/3-1+gap_ind], coeff_lh, chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                       snr_lh1=snr_func(rd_LH1[0:w_lh+nrd/3-1+gap_ind],theta_rd[0:w_lh+nrd/3-1+gap_ind],coeff_lh)
                       theta_lh=theta_rd[0:w_lh+nrd/3-1+gap_ind]
                    endelse
                   
                    if (w_rh-nrd/3 ge 0) then begin
                       if ((w_rh+nrd/3-1 le nrd-1)) then begin
                          fit_rh1=gaussfit(theta_rd[w_rh-nrd/3:w_rh+nrd/3-1],rd_RH1[w_rh-nrd/3:w_rh+nrd/3-1], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                          snr_rh1=snr_func(rd_rH1[w_rh-nrd/3:w_rh+nrd/3-1],theta_rd[w_rh-nrd/3:w_rh+nrd/3-1],coeff_rh)
                          theta_rh=theta_rd[w_rh-nrd/3:w_rh+nrd/3-1]
                       endif else begin
                          gap_ind=w_rh+nrd/3-(nrd-1)
                          fit_rh1=gaussfit(theta_rd[w_rh-nrd/3-gap_ind :nrd-1],rd_LH1[w_rh-nrd/3-gap_ind :nrd-1], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                          snr_rh1=snr_func(rd_RH1[w_rh-nrd/3-gap_ind :nrd-1],theta_rd[w_rh-nrd/3-gap_ind :nrd-1],coeff_rh)
                          theta_rh=theta_rd[w_rh-nrd/3-gap_ind :nrd-1]
                       endelse
                    endif else begin
                       gap_ind=nrd/3-w_rh
                       fit_rh1=gaussfit(theta_rd[0:w_rh+nrd/3-1+gap_ind],rd_RH1[0:w_rh+nrd/3-1+gap_ind], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                       snr_rh1=snr_func(rd_rH1[0:w_rh+nrd/3-1+gap_ind],theta_rd[0:w_rh+nrd/3-1+gap_ind],coeff_rh)
                       theta_rh=theta_rd[0:w_rh+nrd/3-1+gap_ind]
                    endelse

                    if (coeff_lh(1) lt 60.) then coeff_lh(1)=coeff_lh(1)+15. else coeff_lh(1)=coeff_lh(1)+45.
                    if (coeff_rh(1) lt 60.) then coeff_rh(1)=coeff_rh(1)+15. else coeff_rh(1)=coeff_rh(1)+45.

                    tmin=t((j*ni+k1*ni/ni1)) & tmax=t(j*ni+(k1+1)*ni/ni1-1)
                    fmin=f(i*(ni-1-nr)) & fmax=f((i+1)*(ni-1)-i*nr)

                    slope_lh=tan(coeff_lh(1)*!dtor)*dfkhz*ni1/dtmsec ; pente en MHz/s
                    slope_rh=tan(coeff_rh(1)*!dtor)*dfkhz*ni1/dtmsec
                                   
                    if ((snr_lh1 ge 3.) or (snr_rh1 ge 3.)) then begin
                       tag_rh=1 & tag_lh=1
                       nplot=nplot+1
                          
                       plot_data, xlh1, tflh1, rd_lh1, theta_rd, 'LH', slope_lh, snr_lh1, chi2_lh, coeff_lh(2), tmin, tmax, fmin, fmax
                       oplot, theta_lh, fit_lh1, line=2, color=250.
                       oplot,[0,120], [max(rd_lh1), max(rd_lh1)], line=2, color=250.

                       plot_data, xrh1, tfrh1, rd_rh1, theta_rd, 'RH', slope_rh, snr_rh1,chi2_rh, coeff_rh(2), tmin, tmax, fmin, fmax
                       oplot, theta_rh, fit_rh1, line=2, color=250.
                       oplot,[0,120], [max(rd_rh1), max(rd_rh1)], line=2, color=250.
                    endif
                    
                    if (nplot gt 3000) then begin

                       device, /close
                       exit_ps
                       ps_pdf, psnames(-1)+'.ps',/remove
                       spawn,'cp '+psnames(-1)+'.pdf ' +'/home/emauduit/traitement_massif/results/'+ names(-3)+'/plot/'+ names(-2)+'/'+psnames(-1)+'.pdf'
                       spawn,'rm '+psnames(-1)+'.pdf '
                   
                       nf_plot=nf_plot+1
                       psnames=[psnames,names(-1).substring(0,-5)+'_'+strtrim(nc,2)+'_'+strtrim(nf_plot,2)]
                       set_ps,psnames(-1)+'.ps', /landscape
                       !p.multi=[0,3,3,0,0]
                       nplot=0
                 
                    endif
                    
                    
                      
                    params_LH[i,j*ni1+k1,*]=[nf,tag_lh,coeff_lh(0),err_par_lh(0), coeff_lh(1),err_par_lh(1) ,coeff_lh(2),err_par_lh(2), chi2_lh, snr_lh1,err_rd_lh,tmin, tmax, fmin, fmax]
                    params_RH[i,j*ni1+k1,*]=[nf,tag_rh,coeff_rh(0),err_par_rh(0), coeff_rh(1),err_par_rh(1) ,coeff_rh(2),err_par_rh(2), chi2_rh, snr_rh1,err_rd_rh,tmin, tmax, fmin, fmax]

                 endfor

              endfor


           endfor



           device, /close
           exit_ps
           ps_pdf, psnames(-1)+'.ps',/remove
           spawn,'cp '+psnames(-1)+'.pdf ' +'/home/emauduit/traitement_massif/results/'+ names(-3)+'/plot/'+ names(-2)+'/'+psnames(-1)+'.pdf'
           spawn,'rm '+psnames(-1)+'.pdf '
           nf_plot=0

           LH=0 & RH=0
           save, path_file , params_LH, params_RH, file='/home/emauduit/traitement_massif/results/'+ names(-3)+'/parameters/'+ names(-2)+'/parameters_'+names(-1).substring(0,-5)+'_'+strtrim(nc,2)+'.sav'
          
        endfor
        nf_plot=0

        psnames=[psnames,names(-1).substring(0,-5)+'_'+strtrim(n_open,2)+'_'+strtrim(nf_plot,2)]

        set_ps,psnames(-1)+'.ps', /landscape
        device, /color
        !p.multi=[0,3,3,0,0]

        read_junon_data, files[nf], LH, RH, t,f,tmin=tini+n_open*dt_coupure-dt_recouv,fmin=8., fmax=41. 
        dim=size(LH)
        n=dim(1)                
        p=dim(2)                


        header=junoHeader(files[nf])
        fmin=header.freq[0]
        fmax=header.freq[header.nfreq-1]

        if (fmin lt 8. ) then fmin=8.
        if (fmax gt 41.) then fmax=41.

        lfreq=fmax-fmin
        Nc_tot=floor(lfreq*1e3/dfkhz)

        nbandes=ceil(Nc_tot/2975.)
        if (nbandes gt 1) then dfrecouv=((nbandes*2975L)-Nc_tot)*dfkhz*1e-3/(nbandes-1) else dfrecouv=0.

       

        dfreq=(lfreq+((nbandes-1)*dfrecouv))/nbandes 
        ni=round(dfreq*1e3/dfkhz)                    
        while ((ni mod (ni1*ni2*5)) ne 0) do ni=ni-1
        dfreq=dfkhz*ni                       
        nrecouv=round(dfrecouv*1.e3/dfkhz)   


        perte_freq=(Nc_tot+nrecouv*(nbandes-1)+1) mod ni 
        perte_time=n mod ni                              
        dt_recouv=(perte_time+1)*dtmsec
        ncarres=n/ni
        
        p_lf=perte_freq/2
        p_hf=perte_freq/2
        p_lt=perte_time/2
        p_ht=perte_time/2
        if ((perte_freq mod 2) ne 0) then p_hf=p_hf+1
        if ((perte_time mod 2) ne 0) then p_ht=p_ht+1


        LH=LH(0:n-perte_time-1,p_lf :p-p_hf-1)
        RH=RH(0:n-perte_time-1,p_lf :p-p_hf-1)

       
        rho=[0.] & nt=ceil(!pi*ni/(5*ni1))
        theta=findgen(nt)*180./nt
        yc1=prep_ponderation(ni/(5*ni1),ni/(5*ni1))
        wt=where(((theta ge 15.) and (theta le 75.)) or ((theta ge 105.) and (theta le 165.)),nrd)
        theta_rd=findgen(nrd)*120./nrd

       

        params_LH=fltarr(nbandes,ncarres*ni1,15)-450.
        params_RH=fltarr(nbandes,ncarres*ni1,15)-450.



        for i=0,(nbandes-1) do begin

           if i ge 1 then nr=nrecouv else nr=0 
           for j=0,(ncarres-1) do begin

              LH1=LH((j*ni):((j+1)*ni)-1,i*(ni-1-nr):(i+1)*(ni-1)-i*nr)
              RH1=RH((j*ni):((j+1)*ni)-1,i*(ni-1-nr):(i+1)*(ni-1)-i*nr)

           
              LH1=rebin(LH1,ni,ni/ni1)
              RH1=rebin(RH1,ni,ni/ni1)

              for k1=0,ni1-1 do begin
                 
                 traitement_junon,LH1[k1*ni/ni1:(k1+1)*ni/ni1-1,*],yc1,xlh1,tflh1,rd_lh1
                 traitement_junon,RH1[k1*ni/ni1:(k1+1)*ni/ni1-1,*],yc1,xrh1,tfrh1,rd_rh1
                 
                 rd_lh1=smooth(rd_lh1(wt),4, /edge_wrap)
                 rd_rh1=smooth(rd_rh1(wt), 4, /edge_wrap)

                 w_lh=where(rd_LH1 eq max(rd_LH1))
                 w_rh=where(rd_RH1 eq max(rd_RH1))

                 tag_lh=0
                 tag_rh=0
          

                 if (w_lh-nrd/3 ge 0) then begin
                    if ((w_lh+nrd/3-1 le nrd-1)) then begin
                       fit_lh1=gaussfit(theta_rd[w_lh-nrd/3:w_lh+nrd/3-1],rd_LH1[w_lh-nrd/3:w_lh+nrd/3-1], coeff_lh,chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                       snr_lh1=snr_func(rd_LH1[w_lh-nrd/3:w_lh+nrd/3-1],theta_rd[w_lh-nrd/3:w_lh+nrd/3-1],coeff_lh)
                       theta_lh=theta_rd[w_lh-nrd/3:w_lh+nrd/3-1]
                    endif else begin
                       gap_ind=w_lh+nrd/3-(nrd-1)
                       fit_lh1=gaussfit(theta_rd[w_lh-nrd/3-gap_ind :nrd-1],rd_LH1[w_lh-nrd/3-gap_ind :nrd-1], coeff_lh, chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                       snr_lh1=snr_func(rd_LH1[w_lh-nrd/3-gap_ind :nrd-1],theta_rd[w_lh-nrd/3-gap_ind :nrd-1],coeff_lh)
                       theta_lh=theta_rd[w_lh-nrd/3-gap_ind :nrd-1]
                    endelse
                    
                 endif else begin
                    
                    gap_ind=nrd/3-w_lh
                    fit_lh1=gaussfit(theta_rd[0:w_lh+nrd/3-1+gap_ind],rd_LH1[0:w_lh+nrd/3-1+gap_ind], coeff_lh, chisq=chi2_lh, nterms=4, sigma=err_par_lh,yerror=err_rd_lh)
                    snr_lh1=snr_func(rd_LH1[0:w_lh+nrd/3-1+gap_ind],theta_rd[0:w_lh+nrd/3-1+gap_ind],coeff_lh)
                    theta_lh=theta_rd[0:w_lh+nrd/3-1+gap_ind]
                 endelse
                   
                 if (w_rh-nrd/3 ge 0) then begin
                    if ((w_rh+nrd/3-1 le nrd-1)) then begin
                       fit_rh1=gaussfit(theta_rd[w_rh-nrd/3:w_rh+nrd/3-1],rd_RH1[w_rh-nrd/3:w_rh+nrd/3-1], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                       snr_rh1=snr_func(rd_rH1[w_rh-nrd/3:w_rh+nrd/3-1],theta_rd[w_rh-nrd/3:w_rh+nrd/3-1],coeff_rh)
                       theta_rh=theta_rd[w_rh-nrd/3:w_rh+nrd/3-1]
                    endif else begin
                       gap_ind=w_rh+nrd/3-(nrd-1)
                       fit_rh1=gaussfit(theta_rd[w_rh-nrd/3-gap_ind :nrd-1],rd_LH1[w_rh-nrd/3-gap_ind :nrd-1], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                       snr_rh1=snr_func(rd_RH1[w_rh-nrd/3-gap_ind :nrd-1],theta_rd[w_rh-nrd/3-gap_ind :nrd-1],coeff_rh)
                       theta_rh=theta_rd[w_rh-nrd/3-gap_ind :nrd-1]
                    endelse
                 endif else begin
                    gap_ind=nrd/3-w_rh
                    fit_rh1=gaussfit(theta_rd[0:w_rh+nrd/3-1+gap_ind],rd_RH1[0:w_rh+nrd/3-1+gap_ind], coeff_rh, chisq=chi2_rh, nterms=4, sigma=err_par_rh,yerror=err_rd_rh)
                    snr_rh1=snr_func(rd_rH1[0:w_rh+nrd/3-1+gap_ind],theta_rd[0:w_rh+nrd/3-1+gap_ind],coeff_rh)
                    theta_rh=theta_rd[0:w_rh+nrd/3-1+gap_ind]
                 endelse

                 if (coeff_lh(1) lt 60.) then coeff_lh(1)=coeff_lh(1)+15. else coeff_lh(1)=coeff_lh(1)+45.
                 if (coeff_rh(1) lt 60.) then coeff_rh(1)=coeff_rh(1)+15. else coeff_rh(1)=coeff_rh(1)+45.

                 tmin=t((j*ni+k1*ni/ni1)) & tmax=t(j*ni+(k1+1)*ni/ni1-1)
                 fmin=f(i*(ni-1-nr)) & fmax=f((i+1)*(ni-1)-i*nr)

                 slope_lh=tan(coeff_lh(1)*!dtor)*dfkhz*ni1/dtmsec ; pente en MHz/s
                 slope_rh=tan(coeff_rh(1)*!dtor)*dfkhz*ni1/dtmsec
                     

                

                 if ((snr_lh1 ge 3.) or (snr_rh1 ge 3.)) then begin
                    nplot=nplot+1
                    tag_rh=1 & tag_lh=1
                    plot_data, xlh1, tflh1, rd_lh1, theta_rd, 'LH', slope_lh, snr_lh1, chi2_lh, coeff_lh(2), tmin, tmax, fmin, fmax
                    oplot, theta_lh, fit_lh1, line=2, color=250.
                    oplot,[0,120], [max(rd_lh1), max(rd_lh1)], line=2, color=250.

                    plot_data, xrh1, tfrh1, rd_rh1, theta_rd, 'RH', slope_rh, snr_rh1,chi2_rh, coeff_rh(2), tmin, tmax, fmin, fmax
                    oplot, theta_rh, fit_rh1, line=2, color=250.
                    oplot,[0,120], [max(rd_rh1), max(rd_rh1)], line=2, color=250.
                 endif

                 if (nplot gt 3000) then begin

                    device, /close
                    exit_ps
                    ps_pdf, psnames(-1)+'.ps',/remove
                    spawn,'cp '+psnames(-1)+'.pdf ' +'/home/emauduit/traitement_massif/results/'+ names(-3)+'/plot/'+ names(-2)+'/'+psnames(-1)+'.pdf'
                    spawn,'rm '+psnames(-1)+'.pdf '
                   
                    nf_plot=nf_plot+1
                    psnames=[psnames,names(-1).substring(0,-5)+'_'+strtrim(n_open,2)+'_'+strtrim(nf_plot,2)]
                    set_ps,psnames(-1)+'.ps', /landscape
                    !p.multi=[0,3,3,0,0]
                    nplot=0

                    endif
                   
                 params_LH[i,j*ni1+k1,*]=[nf,tag_lh,coeff_lh(0),err_par_lh(0), coeff_lh(1),err_par_lh(1) ,coeff_lh(2),err_par_lh(2), chi2_lh, snr_lh1,err_rd_lh,tmin, tmax, fmin, fmax]
                 params_RH[i,j*ni1+k1,*]=[nf,tag_rh,coeff_rh(0),err_par_rh(0), coeff_rh(1),err_par_rh(1) ,coeff_rh(2),err_par_rh(2), chi2_rh, snr_rh1,err_rd_rh,tmin, tmax, fmin, fmax]


              endfor

           endfor

        endfor

        LH=0 & RH=0
        save, path_file , params_LH, params_RH, file='/home/emauduit/traitement_massif/results/'+ names(-3)+'/parameters/'+ names(-2)+'/parameters_'+names(-1).substring(0,-5)+'_'+strtrim(n_open,2)+'.sav'
          
              
      
     endelse
     
     
     
     times(nf)=toc(clock)
    
    ; save the array containing the parameters in a saveset .sav with the same name than the file
     
     print, 'numero du fichier',nf

     device, /close
     exit_ps
     ps_pdf, psnames(-1)+'.ps',/remove
     spawn,'cp '+psnames(-1)+'.pdf ' +'/home/emauduit/traitement_massif/results/'+ names(-3)+'/plot/'+ names(-2)+'/'+psnames(-1)+'.pdf'
     spawn,'rm '+psnames(-1)+'.pdf '
     
     save, files, sizes, times, filename='/home/emauduit/traitement_massif/results/'+names(-3)+'/liste_fichiers_'+names(-2)+'_'+names(-3)+'.sav'
  endfor

  
  return
end
