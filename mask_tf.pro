;+
; author : Emilie Mauduit
;
; :Description:
; 
;  Function that removes the interferences in a 2D-FFT array
;  
;  :Parameters:
;  
;  INPUT :
;  
;  data : 2D - array 
;  
;  OUTPUT :
;  
;  res : 2D - array of the 2D-FFT whith the interferences removed 
;  
;-


function mask_TF, data

  dim=size(data)                ; shape of the data
  n=dim(1)
  p=dim(2)
  
  x=data

  xt=rebin(x,n,1)  ;integration over frequencies
  
  x=x/rebin(xt, n,p)
  
  xf=rebin(x,1,p) ;integration over time
  
  x=x/rebin(xf,n,p)
  
  return, x
end
