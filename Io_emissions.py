import os
import scipy.io as sio
from scipy.io import readsav
import matplotlib.pyplot as plt
import matplotlib.image as img
import numpy as np

month='06'

phio=[66,100]
cml=[74,184]
jd90=2447892.5000000005

data=readsav(r'C:\Users\Cassiopée05\Documents\Magistère\2020-2021\Stage\Programmes\Traitement massif\interpol_time_'+month+'.sav')

cmlsf_lh=data['cmlsf_lh']
cmlsf_rh=data['cmlsf_rh']
phiosf_lh=data['phiosf_lh']
phiosf_rh=data['phiosf_rh']
phigasf_lh=data['phigasf_lh']
phigasf_rh=data['phigasf_rh']
phieusf_lh=data['phieusf_lh']
phieusf_rh=data['phieusf_rh']
phicasf_lh=data['phicasf_lh']
phicasf_rh=data['phicasf_rh']
phiamsf_lh=data['phiamsf_lh']
phiamsf_rh=data['phiamsf_rh']


t90sf_lh=data['t90sf_lh']
t90sf_rh=data['t90sf_rh']
s90sf_lh=data['s90sf_lh']
s90sf_rh=data['s90sf_rh']
i90sf_lh=data['i90sf_lh']
i90sf_rh=data['i90sf_rh']
snr90sf_lh=data['snr90sf_lh']
snr90sf_rh=data['snr90sf_rh']


t_lh=[]
t_rh=[]
s_lh=[]
s_rh=[]
I_lh=[]
I_rh=[]
snr_lh=[]
snr_rh=[]

dim_lh=np.shape(cmlsf_lh)
dim_rh=np.shape(cmlsf_rh)

nlh=dim_lh[0]
nrh=dim_rh[0]

for i in range (nlh):
    if (((cmlsf_lh[i]>=cml[0]) and (cmlsf_lh[i]<=cml[1])) and (phiosf_lh[i]>=phio[0]) and (phiosf_lh[i]<=phio[1])):
        t_lh.append(t90sf_lh[i]+jd90)
        s_lh.append(s90sf_lh[i])
        I_lh.append(i90sf_lh[i])
        snr_lh.append(snr90sf_lh[i])

for i in range(nrh):
    if (((cmlsf_rh[i]>=cml[0]) and (cmlsf_rh[i]<=cml[1])) and (phiosf_rh[i]>=phio[0]) and (phiosf_rh[i]<=phio[1])):
        t_rh.append(t90sf_rh[i]+jd90)
        s_rh.append(s90sf_rh[i])
        I_rh.append(i90sf_rh[i])
        snr_rh.append(snr90sf_rh[i])


#### Plots #####

hist_s_lh,xs_lh=np.histogram(s_lh,bins=800, range=(-40,40))
hist_s_rh,xs_rh=np.histogram(s_rh,bins=800, range=(-40,40))

hist_I_lh,xi_lh=np.histogram(I_lh,bins=800, range=(np.min(I_lh),np.max(I_lh)))
hist_I_rh,xi_rh=np.histogram(I_rh,bins=800, range=(np.min(I_rh),np.max(I_rh)))

hist_snr_lh, xsnr_lh=np.histogram(snr_lh, bins=1000, range=(0,50))
hist_snr_rh, xsnr_rh=np.histogram(snr_rh, bins=1000, range=(0,50))


plt.figure()
plt.suptitle("Histogrammes - Emission Io B - "+month+'/21 - NDA/JunoN',fontsize='x-large')
plt.subplot(131)
plt.plot(xs_lh[0:-1],hist_s_lh,drawstyle='steps-mid', label='LH')
plt.plot(xs_rh[0:-1],hist_s_rh,drawstyle='steps-mid', label='RH')
plt.xlabel('Pente [MHz/s]')
plt.ylabel('Occurence')
# plt.yscale('log')
plt.grid()
plt.legend()
plt.title('Pentes')
plt.subplot(132)

plt.plot(xi_lh[0:-1],hist_I_lh,drawstyle='steps-mid', label='LH')
plt.plot(xi_rh[0:-1],hist_I_rh,drawstyle='steps-mid',label='RH')
plt.xlabel('Intensité')
plt.ylabel('Occurence')
plt.title('Intensités')
# plt.yscale('log')
plt.grid()
plt.legend()

plt.subplot(133)
plt.plot(xsnr_lh[0:-1], hist_snr_lh, drawstyle='steps-mid', label='LH')
plt.plot(xsnr_rh[0:-1], hist_snr_rh, drawstyle='steps-mid', label='RH')
plt.xlabel('SNR')
plt.ylabel('Occurence')
plt.title('SNR')
# plt.yscale('log')
plt.grid()
plt.legend()

plt.subplots_adjust(bottom=0.5, right=0.95, top=0.9, left=0.05, hspace=0.3, wspace=0.2)
plt.show()


# plt.figure()
# plt.scatter(np.linspace(0,len(t_lh),len(t_lh)),t_lh)
# plt.scatter(np.linspace(0,len(t_rh),len(t_rh)),t_rh)
# plt.show()

