;+
; author : Emilie Mauduit
;
; :Description:
; 
; Retrieve a list of times corresponding to detections for a given month. Then, interpolates a list of times corresponding to detections with Io, Ganymède, Europa, Callisto, Amalthea phases and  CML coordinates (central meridian longitude).
; 
; :Parameters:
; 
; INPUT:
; 
; folder : path of the directory containing the files to analyse
; ceil_SNR : threshold of SNR below which detections are excluded
; nrebin : 7 ou 17 , integration factor used for the data analysis
; 
; OUTPUT :
;
; cmlsf_lh, cmlsf_rh : CML coordinates corresponding to detections for each polarization
; phiosf_lh, phiosf_rh : Io phase corresponding to detections for each polarization
; phigasf_lh, phigasf_rh : Ganymede phase corresponding to detections for each polarization
; phieusf_lh, phieusf_rh : Europa phase corresponding to detections for each polarization
; phicasf_lh, phicasf_rh : Callisto phase corresponding to detections for each polarization
; phiamsf_lh, phiamsf_rh : Amalthea phase corresponding to detections for each polarization
; t90sf_lh, t90sf_rh : Times corresponding to detections for each polarization
; s90sf_lh,s90sf_rh : Drifts values corresponding to detections for each polarization
; I90sf_lh,I90sf_rh : Intensities corresponding to detections for each polarization
; snr90sf_lh, snr90sf_rh : SNR corresponding to detections for each polarization
;
; Keywords:
;
; slopes : array of two elements containing the minimum and maximum values of drifts w want to consider
; Imax : array of two elements containing the minimum and maximum values of intensities w want to consider
;
; Uses : times_sf.pro
;-
;

pro fig_ephem, folder, ceil_SNR,nrebin,SLOPES=slopes,IMAX=Imax

  files=file_Search(folder+'\*.sav', count=nfiles)
  
  t90sf_lh=[-1]
  t90sf_rh=[-1]
  s90sf_lh=[-1]
  s90sf_rh=[-1]
  I90sf_lh=[-1]
  I90sf_rh=[-1]
  snr90sf_lh=[-1]
  snr90sf_rh=[-1]
  
  
  for i=0,nfiles-1 do begin
    if (keyword_set(slopes) and keyword_set(Imax)) then times_sf, files[i], ceil_SNR, nrebin, tsf_lh, tsf_rh,ssf_lh, ssf_rh, Isf_lh, Isf_rh,snrsf_lh, snrsf_rh, slopes=slopes, imax=imax
    if (keyword_set(slopes)) then times_sf, files[i], ceil_SNR, nrebin, tsf_lh, tsf_rh,ssf_lh, ssf_rh, Isf_lh, Isf_rh, snrsf_lh, snrsf_rh,slopes=slopes
    if (keyword_set(imax)) then times_sf, files[i], ceil_SNR, nrebin, tsf_lh, tsf_rh,ssf_lh, ssf_rh, Isf_lh, Isf_rh,snrsf_lh, snrsf_rh, imax=imax
    if ((not keyword_set(slopes)) and (not keyword_set(imax))) then times_sf, files[i], ceil_SNR, nrebin, tsf_lh, tsf_rh,ssf_lh, ssf_rh, Isf_lh, Isf_rh,snrsf_lh, snrsf_rh
    
    t90sf_lh=[t90sf_lh, tsf_lh]
    t90sf_rh=[t90sf_rh, tsf_rh]
    s90sf_lh=[s90sf_lh,ssf_lh]
    s90sf_rh=[s90sf_rh,ssf_rh]
    I90sf_lh=[I90sf_lh,Isf_lh]
    I90sf_rh=[I90sf_rh,Isf_rh]
    snr90sf_lh=[snr90sf_lh,snrsf_lh]
    snr90sf_rh=[snr90sf_rh, snrsf_rh]
    
  endfor
  
  t90sf_lh=t90sf_lh[1:*]
  t90sf_rh=t90sf_rh[1:*]
  s90sf_lh=s90sf_lh[1:*]
  s90sf_rh=s90sf_rh[1:*]
  I90sf_lh=I90sf_lh[1:*]
  I90sf_rh=I90sf_rh[1:*]
  snr90sf_lh=snr90sf_lh[1:*]
  snr90sf_rh=snr90sf_rh[1:*]
  
  restore,'C:\Users\Cassiopée05\Documents\Magistère\2020-2021\Stage\Programmes\catalogue-Jupiter-Nancay\ephem\ephem.sav'
  
  cmlsf_lh=interpol(cml,t,t90sf_lh) mod 360
  cmlsf_rh=interpol(cml,t,t90sf_rh) mod 360
  
  phio=cml+180.-lio
  phiosf_lh=interpol(phio,t,t90sf_lh) mod 360
  phiosf_rh=interpol(phio,t,t90sf_rh) mod 360
  
  phiga=cml+180.-lga
  phigasf_lh=interpol(phiga,t,t90sf_lh) mod 360
  phigasf_rh=interpol(phiga,t,t90sf_rh) mod 360
  
  phieu=cml+180.-leu
  phieusf_lh=interpol(phieu,t,t90sf_lh) mod 360
  phieusf_rh=interpol(phieu,t,t90sf_rh) mod 360
  
  phica=cml+180.-lca
  phicasf_lh=interpol(phica, t, t90sf_lh) mod 360
  phicasf_rh=interpol(phica, t, t90sf_rh) mod 360
  
  phiam=cml+180.-lam
  phiamsf_lh=interpol(phiam, t, t90sf_lh) mod 360
  phiamsf_rh=interpol(phiam, t, t90sf_rh) mod 360
 
  
  names=folder.split('\\')
  save, cmlsf_lh, cmlsf_rh, phiosf_lh, phiosf_rh, phigasf_lh, phigasf_rh, phieusf_lh, phieusf_rh, phicasf_lh, phicasf_rh, phiamsf_lh, phiamsf_rh, t90sf_lh,t90sf_rh,s90sf_lh,s90sf_rh,I90sf_lh,I90sf_rh,snr90sf_lh, snr90sf_rh, filename='interpol_time_'+names(-1)+'.sav'

end
