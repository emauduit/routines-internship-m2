Processing programs :

- deparasitage.pro => remove interferences in a 2D dynamical spectra
- mask_tf.pro => remove residual interferences in the 2D-FFT
- traitement_junon.pro => process a dynamical spectra to obtain its Radon transform
- snr_func.pro => computes the signal to noise ratio of a given 1D spectra
- plot_data.pro => plots the dynamical spectra, its 2D-FFT and its Radon transform
- read_junon_data.pro => opens and reads a JunoN dataset
- junon_sampling.pro => process a given dataset,
- traitement_massif.pro => processes all the existent datasets

Analysis programs :

- times_sf.pro => retieves the times, satellite phases and observer's longitude corresponding to every detections of fine structures
- plot_ephem.py => plots the previous parameters on probability maps of emissions
- cartes.py => functions used in plot_ephem.py
- gan_emissions.py => plots histogram for various parameters of the detections like drifts, intensity and SNR, for a given group of detections of Ganymede emissions
- io_emissions.py => Same for Io

