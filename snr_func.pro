;+
; author : Emilie MAUDUIT
;
; :Description:
; 
;  Compute the signal to noise ratio of a 1D array
;  
;   INPUT : 
;
; x : 1D array
; angles : 1D array containing the abscissa 
; coeff : parameters of the gaussian fit
;   
;   OUTPUT : ratio between spike amplitude - median value of the spectra and standard deviation of the spectra. 
;   
;   
;-


function snr_func, x, angles, coeff

  ampl=max(x)
  wm=coeff(1)
  w=where((angles le wm-(2*coeff(2))) or (angles ge wm+(2*coeff(2))),nw)

  if (nw ne 0) then begin
     m=median(x(w))
     std=stddev(x(w))
  endif else begin
     m=median(x)
     std=stddev(x)
  endelse

  result=(ampl-m)/std
  if (max(x) ge (m + 2*stddev(x))) then begin
  return, result
  endif else begin
     return, -450.
  endelse
  
end
