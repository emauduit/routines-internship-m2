;+
;
; author : Emilie MAUDUIT
;
; : Description:
;
; Reads a .sav file containing the parameters obtained with the processing and retreives the median time of the panels where there is a fine 
; structure detected
;
; :Parameters:
;
; INPUT:
; path : path of the file to analyze
; ceil_SNR : threshold value of the SNR under which the detectionis excluded
; nrebin : value of the integrating factor (here 7)
;
; OUTPUT:
; tsf_lh : times list (in Julian days since 01/01/1990) satisfying the criteria for the left handed polarization
; tsf_rh : same for the right handed polarization
;
; Keywords:
;
; slopes : array of two elements containing the boundaries of slopes we want to look for
; Imax : same for intensities
;
;-





pro times_sf, path ,ceil_SNR,nrebin,tsf_lh, tsf_rh, ssf_lh, ssf_rh, Isf_lh, Isf_rh, snrsf_lh, snrsf_rh,SLOPES=slopes, Imax=Imax

  restore, 'C:\Users\Cassiopée05\Documents\Magistère\2020-2021\Stage\Programmes\Lecture\tag2.sav'
  
  restore, path
  dim=size(params_lh)
  n=dim(1)
  p=dim(2)
  names=path.split('\\')
  yy=names(-4)
  mm=double(names(-2))
  dd=names(-1).substring(17,18)

  tsf_lh=[-1]
  tsf_rh=[-1]
  ssf_lh=[-1]
  ssf_rh=[-1]
  Isf_lh=[-1]
  Isf_rh=[-1]
  snrsf_lh=[-1]
  snrsf_rh=[-1]

if (keyword_set(Imax) and keyword_set(slopes)) then begin
    for k=0,n-1 do begin
      wlh=where((params_lh[k,*,9] ge ceil_SNR) and (params_lh[k,*,2] ge Imax(0)) and (params_lh[k,*,2] le Imax(1)) and (tan(params_lh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec ge slopes(0)) and (tan(params_lh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec le slopes(1)))
      tmin=reform(params_lh[k,wlh,11], n_elements(params_lh[k,wlh,11]))
      tmax=reform(params_lh[k,wlh,12], n_elements(params_lh[k,wlh,12]))
      angle_lh=reform(params_lh[k,wlh,4],n_elements(params_lh[k,wlh,4]))
      I_lh=reform(params_lh[k,wlh,2],n_elements(params_lh[k,wlh,2]))
      snr_lh=reform(params_lh[k,wlh,9], n_elements(params_lh[k,wlh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_lh=[tsf_lh,t90temp]
      ssf_lh=[ssf_lh,tan(angle_lh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_lh=[Isf_lh,I_lh]
      snrsf_lh=[snrsf_lh,snr_lh]

      wrh=where((params_rh[k,*,9] ge ceil_SNR) and (params_rh[k,*,2] ge Imax(0)) and (params_rh[k,*,2] le Imax(1)) and (tan(params_rh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec ge slopes(0)) and (tan(params_rh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec le slopes(1)))
      tmin=reform(params_rh[k,wrh,11], n_elements(params_rh[k,wrh,11]))
      tmax=reform(params_rh[k,wrh,12], n_elements(params_rh[k,wrh,12]))
      angle_rh=reform(params_rh[k,wrh,4],n_elements(params_rh[k,wrh,4]))
      I_rh=reform(params_rh[k,wrh,2],n_elements(params_rh[k,wrh,2]))
      snr_rh=reform(params_rh[k,wrh,9], n_elements(params_rh[k,wrh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_rh=[tsf_rh,t90temp]
      ssf_rh=[ssf_rh,tan(angle_rh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_rh=[Isf_rh,I_rh]
      snrsf_rh=[snrsf_rh,snr_rh]

  endfor

endif

if keyword_set(slopes) then begin
    for k=0,n-1 do begin
      wlh=where((params_lh[k,*,9] ge ceil_SNR) and (tan(params_lh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec ge slopes(0)) and (tan(params_lh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec le slopes(1)))
      tmin=reform(params_lh[k,wlh,11], n_elements(params_lh[k,wlh,11]))
      tmax=reform(params_lh[k,wlh,12], n_elements(params_lh[k,wlh,12]))
      angle_lh=reform(params_lh[k,wlh,4],n_elements(params_lh[k,wlh,4]))
      I_lh=reform(params_lh[k,wlh,2],n_elements(params_lh[k,wlh,2]))
      snr_lh=reform(params_lh[k,wlh,9], n_elements(params_lh[k,wlh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_lh=[tsf_lh,t90temp]
      ssf_lh=[ssf_lh,tan(angle_lh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_lh=[Isf_lh,I_lh]
      snrsf_lh=[snrsf_lh,snr_lh]

      wrh=where((params_rh[k,*,9] ge ceil_SNR) and (tan(params_rh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec ge slopes(0)) and (tan(params_rh[k,*,4]*!dtor)*nrebin*dfkhz/dtmsec le slopes(1)))
      tmin=reform(params_rh[k,wrh,11], n_elements(params_rh[k,wrh,11]))
      tmax=reform(params_rh[k,wrh,12], n_elements(params_rh[k,wrh,12]))
      angle_rh=reform(params_rh[k,wrh,4],n_elements(params_rh[k,wrh,4]))
      I_rh=reform(params_rh[k,wrh,2],n_elements(params_rh[k,wrh,2]))
      snr_rh=reform(params_rh[k,wrh,9], n_elements(params_rh[k,wrh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_rh=[tsf_rh,t90temp]
      ssf_rh=[ssf_rh,tan(angle_rh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_rh=[Isf_rh,I_rh]
      snrsf_rh=[snrsf_rh,snr_rh]
      
    endfor
  
endif 

if keyword_set(Imax) then begin
    for k=0,n-1 do begin
      wlh=where((params_lh[k,*,9] ge ceil_SNR) and (params_lh[k,*,2] ge Imax(0)) and (params_lh[k,*,2] le Imax(1)))
      tmin=reform(params_lh[k,wlh,11], n_elements(params_lh[k,wlh,11]))
      tmax=reform(params_lh[k,wlh,12], n_elements(params_lh[k,wlh,12]))
      angle_lh=reform(params_lh[k,wlh,4],n_elements(params_lh[k,wlh,4]))
      I_lh=reform(params_lh[k,wlh,2],n_elements(params_lh[k,wlh,2]))
      snr_lh=reform(params_lh[k,wlh,9], n_elements(params_lh[k,wlh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_lh=[tsf_lh,t90temp]
      ssf_lh=[ssf_lh,tan(angle_lh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_lh=[Isf_lh,I_lh]
      snrsf_lh=[snrsf_lh,snr_lh]

      wrh=where((params_rh[k,*,9] ge ceil_SNR) and (params_rh[k,*,2] ge Imax(0)) and (params_rh[k,*,2] le Imax(1)))
      tmin=reform(params_rh[k,wrh,11], n_elements(params_rh[k,wrh,11]))
      tmax=reform(params_rh[k,wrh,12], n_elements(params_rh[k,wrh,12]))
      angle_rh=reform(params_rh[k,wrh,4],n_elements(params_rh[k,wrh,4]))
      I_rh=reform(params_rh[k,wrh,2],n_elements(params_rh[k,wrh,2]))
      snr_rh=reform(params_rh[k,wrh,9], n_elements(params_rh[k,wrh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_rh=[tsf_rh,t90temp]
      ssf_rh=[ssf_rh,tan(angle_rh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_rh=[Isf_rh,I_rh]
      snrsf_rh=[snrsf_rh,snr_rh]

    endfor

endif

if ((not keyword_set(slopes)) and (not keyword_set(Imax))) then begin
    for k=0,n-1 do begin
      wlh=where(params_lh[k,*,9] ge ceil_SNR)
      tmin=reform(params_lh[k,wlh,11], n_elements(params_lh[k,wlh,11]))
      tmax=reform(params_lh[k,wlh,12], n_elements(params_lh[k,wlh,12]))
      angle_lh=reform(params_lh[k,wlh,4],n_elements(params_lh[k,wlh,4]))
      I_lh=reform(params_lh[k,wlh,2],n_elements(params_lh[k,wlh,2]))
      snr_lh=reform(params_lh[k,wlh,9], n_elements(params_lh[k,wlh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_lh=[tsf_lh,t90temp]
      ssf_lh=[ssf_lh,tan(angle_lh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_lh=[Isf_lh,I_lh]
      snrsf_lh=[snrsf_lh,snr_lh]

      wrh=where(params_rh[k,*,9] ge ceil_SNR)
      tmin=reform(params_rh[k,wrh,11], n_elements(params_rh[k,wrh,11]))
      tmax=reform(params_rh[k,wrh,12], n_elements(params_rh[k,wrh,12]))
      angle_rh=reform(params_rh[k,wrh,4],n_elements(params_rh[k,wrh,4]))
      I_rh=reform(params_rh[k,wrh,2],n_elements(params_rh[k,wrh,2]))
      snr_rh=reform(params_rh[k,wrh,9], n_elements(params_rh[k,wrh,9]))
      ttemp=(tmax+tmin)/2.
      t90temp=julday(mm,dd,yy,ttemp)-julday(1,1.d0,1990,0.)
      tsf_rh=[tsf_rh,t90temp]
      ssf_rh=[ssf_rh,tan(angle_rh*!dtor)*nrebin*dfkhz/dtmsec]
      Isf_rh=[Isf_rh,I_rh]
      snrsf_rh=[snrsf_rh,snr_rh]

    endfor
endif

tsf_lh=tsf_lh[1:*]
tsf_rh=tsf_rh[1:*]
ssf_lh=ssf_lh[1:*]
ssf_rh=ssf_rh[1:*]
Isf_lh=Isf_lh[1:*]
Isf_rh=Isf_rh[1:*]
snrsf_lh=snrsf_lh[1:*]
snrsf_rh=snrsf_rh[1:*]

return
end
