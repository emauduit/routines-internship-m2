;+
;
; author : Emilie MAUDUIT
;
; :Decription:
;
; Massive data processing
;
;-

  years=['2021','2020','2019','2018','2017']
  months=['01','02','03','04','05','06','07','08','09','10','11','12']

  path='/databf/nda/Juno/'

  for i=0, n_elements(years)-1 do begin
     for j=0, n_elements(months)-1 do begin

        folderpath=path+years(i)+'/'+months(j)
        junon_sampling, folderpath, 4, 1., 7, 17
        
     endfor
     
  endfor
  

end
