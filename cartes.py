import os
import scipy.io as sio
from scipy.io import readsav
import matplotlib.pyplot as plt
import matplotlib.image as img
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np


data1=readsav(r'C:\Users\Cassiopée05\Documents\Magistère\2020-2021\Stage\Programmes\Traitement massif\probas_cartes.sav')

carte_io=data1['probaio']
carte_gan=data1['probagan']

max_io=np.quantile(carte_io,0.99)
max_gan=np.quantile(carte_gan,0.95)



## Contours Io

IoAcml=[205.,220,220,240,240,245,245,265,265,270,270,275,275,245,245,215,215,200,200,190,190,185,185,190,190,195,195,200,200,205,205]
IoAphi=[185.,185,190,190,195,195,200,200,205,205,230,230,265,265,260,260,255,255,250,250,245,245,230,230,225,225,205,205,190,190,185]
IoA1cml=[205.,225,225,255,255,265,265,235,235,210,210,200,200,205,205]
IoA1phi=[160.,160,165,165,170,170,195,195,190,190,185,185,165,165,160]
IoA2cml=[315.,335,335,345,345,325,325,300,300,305,305,315,315]
IoA2phi=[220.,220,225,225,240,240,245,245,230,230,225,225,220]
IoBcml=[ 90.,105,105,115,115,130,130,155,155,175,175,185,185,195,195,200,200,180,180,110,110, 95, 95, 90, 90, 80, 80, 75, 75, 90, 90]
IoBphi=[ 55., 55, 70, 70, 75, 75, 80, 80, 85, 85, 90, 90, 95, 95,100,100,115,115,110,110,105,105,100,100, 95, 95, 90, 90, 65, 65, 55]
IoB1cml=[130.,165,165,175,175,180,180,185,185,165,165,145,145,130,130]
IoB1phi=[ 65., 65, 70, 70, 75, 75, 80, 80, 95, 95, 90, 90, 85, 85, 65]
IoCcml=[290.,310,310,320,320,345,345,360,360,315,315,295,295,290,290,275,275,270,270,280,280,290,290]
IoCphi=[215.,215,220,220,225,225,235,235,260,260,255,255,250,250,245,245,240,240,225,225,220,220,215]
IoC0cml=[  0., 45, 45, 20, 20,  0,  0]
IoC0phi=[240.,240,255,255,260,260,240]
IoDcml=[ 20., 55, 55,175,175,190,190,230,230,185,185,115,115, 30, 30, 20, 20]
IoDphi=[ 95., 95,100,100,105,105,110,110,125,125,120,120,115,115,110,110, 95]

## Contour Ganymède

GaAcml=[285.,340,340,285,285]
GaAphi=[195.,195,240,240,195]
GaBcml=[110.,175,175,110,110]
GaBphi=[ 75., 75,100,100, 75]
GaB1cml=[125.,170,170,125,125]
GaB1phi=[ 25., 25, 75, 75, 25]
GaCcml=[315.,360,360,315,315]
GaCphi=[235.,235,260,260,235]
GaC0cml=[  0., 40, 40, 0,  0]
GaC0phi=[240.,240,260,260,240]
GaDcml=[125.,180,180,125,125]
GaDphi=[190.,190,210,210,190]

#cmap1=copy(copy(plt.cm.nipy_spectral))


####### IO-CML #####


def io(x,y, legends, markers):
    ax=plt.gca()
    im=ax.imshow(carte_io, origin='lower',extent=[0,360,0,360], vmax=max_io,cmap='jet', alpha=0.5)
    plt.xlabel('CML (°)')
    plt.ylabel('Io phase (°)')
    plt.title(legends)
    plt.plot(x, y, markers,color='black')
    plt.plot(IoAcml,IoAphi, '-', color='white')
    plt.plot(IoA1cml,IoA1phi, '--', color='white')
    plt.plot(IoA2cml,IoA2phi, '--', color='white')
    plt.plot(IoBcml,IoBphi, '-', color='white')
    plt.plot(IoB1cml,IoB1phi, '--', color='white')
    plt.plot(IoCcml,IoCphi, '-', color='white')
    plt.plot(IoC0cml,IoC0phi, '--', color='white')
    plt.plot(IoDcml,IoDphi, '-', color='white')
    plt.text(225,275,'A',color='white', fontsize='large',fontweight='demibold')
    plt.text(170,170,"A'",color='white', fontsize='large',fontweight='demibold')
    plt.text(315,195,'A"',color='white', fontsize='large',fontweight='demibold')
    plt.text(50,60,'B',color='white', fontsize='large',fontweight='demibold')
    plt.text(185,60,"B'",color='white', fontsize='large',fontweight='demibold')
    plt.text(310,270,'C',color='white', fontsize='large',fontweight='demibold')
    plt.text(5,270,'C',color='white', fontsize='large',fontweight='demibold')
    plt.text(60,125,'D',color='white', fontsize='large',fontweight='demibold')
    plt.xlim(0,360)
    plt.ylim(0,360)
    #plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb=plt.colorbar(im, cax=cax)
    cb.set_label('%', fontsize='large')
    return()


####### GAN-CML ######

def gan(x,y, cmlio, phio, cml, phi ,legends, markers):
    ax=plt.gca()
    im=ax.imshow(carte_gan, origin='lower',extent=[0,360,0,360],vmax=max_gan,cmap='jet',alpha=0.5)
    plt.xlabel('CML (°)')
    plt.ylabel('Ganymede phase (°)')
    plt.title(legends)
    dim=np.shape(x)
    n=dim[0]
    xio=[]
    yio=[]
    xga=[]
    yga=[]
    k=0

    while (k < len(cml)):
        print(k)
        if (cml[k] <= cml[k+1]):
            for i in range (n):
                if (((cmlio[i]>=cml[k]) and (cmlio[i]<=cml[k+1])) and (phio[i]>=phi[k]) and (phio[i]<=phi[k+1])):
                    xio.append(x[i])
                    yio.append(y[i])
                else:
                    xga.append(x[i])
                    yga.append(y[i])
        else:
            for i in range (n):
                if (((cmlio[i]>=cml[k]) or (cmlio[i]<=cml[k+1])) and (phio[i]>=phi[k]) and (phio[i]<=phi[k+1])):
                    xio.append(x[i])
                    yio.append(y[i])
                else:
                    xga.append(x[i])
                    yga.append(y[i])
        k=k+2
    print(k)
    plt.plot(xga, yga,markers, color='red', label='Non-Io')
    plt.plot(xio, yio, markers,color='black', label='Io')
    plt.plot(GaAcml, GaAphi,'-', color='white')
    plt.plot(GaBcml, GaBphi,'-', color='white')
    plt.plot(GaB1cml, GaB1phi,'--', color='white')
    plt.plot(GaCcml, GaCphi,'-', color='white')
    plt.plot(GaC0cml, GaC0phi,'--', color='white')
    plt.plot(GaDcml, GaDphi,'-', color='white')
    plt.text(335,175,'A',color='white', fontsize='large',fontweight='demibold')
    plt.text(120,105,'B',color='white', fontsize='large',fontweight='demibold')
    plt.text(180,50,"B'",color='white', fontsize='large',fontweight='demibold')
    plt.text(345,270,'C',color='white', fontsize='large',fontweight='demibold')
    plt.text(10,270,'C',color='white', fontsize='large',fontweight='demibold')
    plt.text(100,190,'D',color='white', fontsize='large',fontweight='demibold')
    plt.xlim(0,360)
    plt.ylim(0,360)
    plt.legend()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb=plt.colorbar(im, cax=cax)
    cb.set_label('%', fontsize='large')
    return()

#### Europa- CML

def europa(x,y,cmlio, phio, cmlga, phiga, cmli, phii, cmlg, phig, legends, markers):
    ax=plt.gca()
    im=plt.imshow(carte_gan, origin='lower',extent=[0,360,0,360],vmax=max_gan,cmap='jet', alpha=0.5)
    plt.xlabel('CML (°)')
    plt.ylabel('Europa phase (°)')
    dim=np.shape(x)
    n=dim[0]
    xio=[]
    yio=[]
    xga=[]
    yga=[]
    xeu=[]
    yeu=[]
    k=0

    while (k < len(cmli)):
        print(k)
        if (cmli[k] <= cmli[k+1]):
            for i in range (n):
                if (((cmlio[i]>=cmli[k]) and (cmlio[i]<=cmli[k+1])) and (phio[i]>=phii[k]) and (phio[i]<=phii[k+1])):
                    xio.append(x[i])
                    yio.append(y[i])
        else:
            for i in range (n):
                if (((cmlio[i]>=cmli[k]) or (cmlio[i]<=cmli[k+1])) and (phio[i]>=phii[k]) and (phio[i]<=phii[k+1])):
                    xio.append(x[i])
                    yio.append(y[i])
        k=k+2
    print(k)
    k=0
    while (k < len(cmlg)):
        print(k)
        if (cmlg[k] <= cmlg[k+1]):
            for i in range (n):
                if (((cmlga[i]>=cmlg[k]) and (cmlga[i]<=cmlg[k+1])) and (phiga[i]>=phig[k]) and (phiga[i]<=phig[k+1])):
                    xga.append(x[i])
                    yga.append(y[i])
                else:
                    if (x[i] not in xio):
                        xeu.append(x[i])
                        yeu.append(y[i])
        else:
            for i in range (n):
                if (((cmlga[i]>=cmlg[k]) or (cmlga[i]<=cmlg[k+1])) and (phiga[i]>=phig[k]) and (phiga[i]<=phig[k+1])):
                    xga.append(x[i])
                    yga.append(y[i])
                else:
                    if (x[i] not in xio):
                        xeu.append(x[i])
                        yeu.append(y[i])
        k=k+2
    print(k)
    plt.plot(xeu, yeu, markers,color='fuchsia', label='Non-Io, Non-Gan')
    plt.plot(xga, yga,markers, color='red', label='Non-Io')
    plt.plot(xio, yio, markers,color='black', label='Io')
    plt.xlim(0,360)
    plt.ylim(0,360)
    plt.legend()
    plt.title(legends)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb=plt.colorbar(im, cax=cax)
    cb.set_label('%', fontsize='large')


    return()


#### Callisto - CML

def callisto(x,y,title, legends, markers):
    plt.imshow(carte_gan, origin='lower',extent=[0,360,0,360],vmax=max_gan,cmap='jet')
    plt.xlabel('CML (°)')
    plt.ylabel('Calisto phase (°)')
    for i in range (0,len(x)):
        plt.plot(x[i], y[i], markers[i], label=legends[i],color='black')
    plt.xlim(0,360)
    plt.ylim(0,360)
    cb=plt.colorbar()
    cb.set_label('%', fontsize='large')
    plt.legend()
    plt.title(title)
    return()

#### Amalthea - CML

def amalthea(x,y,title, legends, markers):
    plt.imshow(carte_gan, origin='lower',extent=[0,360,0,360],vmax=max_gan,cmap='jet')
    plt.xlabel('CML (°)')
    plt.ylabel('Amaltha phase (°)')
    for i in range (0,len(x)):
        plt.plot(x[i], y[i], markers[i], label=legends[i],color='black')
    plt.xlim(0,360)
    plt.ylim(0,360)
    cb=plt.colorbar()
    cb.set_label('%', fontsize='large')
    plt.legend()
    plt.title(title)
    return()