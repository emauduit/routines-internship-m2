;+
; :Description:
;     Reads JunoN data acquired with the JunoN t-f mode
;
; :Author:
;    Laurent Lamy
;
; :History:
;    2021/03/22: Created
;
;+
; :Uses:
;    junolib
;
; :Params:
;    file :    in, required, type=string array
;       path of the data file
;
;-


PRO read_junon_data,file,LH,RH,t,f,mn=mn,sec=sec,tmin=tmin,tmax=tmax,fmin=fmin,fmax=fmax

tic
if file_test(file) ne 1   then return
if ~keyword_set(mn) and ~keyword_set(sec) then hour = 1
if keyword_set(hour)      then begin & tunit = 'h'     & convert = 1.    & endif
if keyword_set(mn)        then begin & tunit = 'min'   & convert = 60.   & endif
if keyword_set(sec)       then begin & tunit = 'sec'   & convert = 3600. & endif
yy=fix(strmid(file,27,4,/rev)) 
mo=fix(strmid(file,23,2,/rev))              
dd=fix(strmid(file,21,2,/rev))     
jd0=julday(mo,dd,yy,0.,0.,0.)

; Reading the header : 
print,'Reading file ',file
h = junoheader(file)
nf = h.nfreq
f = h.freq
dt = h.dt*1e-3
nchan = h.nbchan



; Adjusting the frequency range and integration factor : 
if keyword_set(fmin) and keyword_set(fmax) then wf = where(f ge fmin and f le fmax,nf) else $ ; MHz
wf = where(f ge min(f) and f le max(f),nf) 
f = f(wf)

; Reading the data :
openr, lun, /GET_LUN, file
eCube = {magic:0UL, id:0UL, $
         date:{iJD:0UL, iSec:0UL, nSub:0UL, dSub:0UL}, $
         undef0:0UL, undef1:0UL, $
         corr:replicate({magic:0UL, no:0UL, data:fltarr(h.nfreq)},nchan) }

ptr0 = junoGetPosFirstCube(file)  ; size of the header
sizecube = long64(N_Tags(eCube, /LENGTH)) ; size of 1 record/spectrum
sizedata = (fStat(lun)).Size      ; size of the full dataset (data + header)
ncubes = (sizedata - ptr0)/sizecube-1L  ; number of records/spectra
if ncubes eq 0 then return 
point_lun,lun,ptr0 ; points to the first record/spectrum after the header
readu,lun,ecube    ; reads the first record/spectrum
jd1 = junoDate(ecube.date)

; Adjusting the temporal range : 
trec = dindgen(ncubes)*dt/86400.d0+jd1
if ~keyword_set(tmin) then tmin=min((trec-jd0)*24.d0*convert)
if ~keyword_set(tmax) then tmax=max((trec-jd0+dt/86400.d0)*24.d0*convert)
wt = where((trec-jd0)*24.d0*convert ge tmin and (trec-jd0)*24.d0*convert lt tmax,nt)
;trec = trec(wt)

; duration = nt*dt/3600d0*double(convert)
if wt(0) ne -1 then k1 = wt(0) else stop,'Unidentified interval'
ptri = k1*sizecube + ptr0

tmpcube = replicate(ecube,nt)
point_lun,lun,long64(ptri)
readu,lun,tmpcube
LH = (transpose(tmpcube.corr[0].data))[*,wf]
RH = (transpose(tmpcube.corr[3].data))[*,wf]
;V = (LH-RH)/(LH+RH)
t  = (junoDate(tmpcube.date)-jd0)*24.d0*convert    

wt = where(t*convert ge tmin and t*convert lt tmax,nt)
LH = LH(wt,*)
RH = RH(wt,*)
t = t(wt)

if keyword_set(lun) then begin
  close, lun
  free_Lun, lun
endif


return
toc
end
