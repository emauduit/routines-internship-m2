;+
; author : Emilie Mauduit
;
; :Description:
;
; Function which erase the interferences higher than threshold*sigma in a given array.
;
; :Parameters:
;
; INPUT :
; 
; data : a two dimensional array
; seuil : threshold value for the removing
;
; OUTPUT :
;
; x : the input array, with interferences removed
;
; Uses : le_auto_.pro, interpol_ze.pro and make_background.pro
;
;
;
;-
;

function deparasitage, data, seuil

  dim=size(data)
  n=dim(1)
  p=dim(2)

  x=data
  xdb=10.*alog10(x)

 ; integration of the array over each axes

  xt=rebin(xdb,n,1)
  xf=rebin(xdb,1,p)

 ; identifying the indices with interferences

  le_auto_,xt,51,seuil,1,xnett,pt, edge=2,npix=1
  le_auto_,xf,51,seuil,1,xnetf, pf, edge=2,npix=1

 ; replacing the value in the indices by zeros and interpolating over thoses values each lines and columns

  x(where(pt eq 0),*)=0
  x(*,where(pf eq 0))=0
  for i=0,p-1 do begin
    xx=x(*,i)
    interpol_ze,xx
    x(*,i)=xx
  endfor
  for i=0,n-1 do begin
    xx=x(i,*)
    interpol_ze,xx
    x(i,*)=xx
  endfor

 ; dividing the resulting array by its background

  MAKE_BACKGROUND,x,'',b,s
  x=x/rebin(reform(b,1,p),n,p)

  return, x
end
