import os
import scipy.io as sio
from scipy.io import readsav
import matplotlib.pyplot as plt
import matplotlib.image as img
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import cartes


month='04'
data2=readsav(r'C:\Users\Cassiopée05\Documents\Magistère\2020-2021\Stage\Programmes\Traitement massif\interpol_time_'+month+'.sav')

cmlsf_lh=data2['cmlsf_lh']
cmlsf_rh=data2['cmlsf_rh']
# cmlsf_lh17=data2['cmlsf_lh17']
# cmlsf_rh17=data2['cmlsf_rh17']
phiosf_lh=data2['phiosf_lh']
phiosf_rh=data2['phiosf_rh']
# phiosf_lh17=data2['phiosf_lh17']
# phiosf_rh17=data2['phiosf_rh17']
phigasf_lh=data2['phigasf_lh']
phigasf_rh=data2['phigasf_rh']
# phigasf_lh17=data2['phigasf_lh17']
# phigasf_rh17=data2['phigasf_rh17']
phieusf_lh=data2['phieusf_lh']
phieusf_rh=data2['phieusf_rh']

phicasf_lh=data2['phicasf_lh']
phicasf_rh=data2['phicasf_rh']

phiamsf_lh=data2['phiamsf_lh']
phiamsf_rh=data2['phiamsf_rh']

cml=[cmlsf_lh,cmlsf_rh]
phio=[phiosf_lh,phiosf_rh]
phiga=[phigasf_lh,phigasf_rh]
phieu=[phieusf_lh,phieusf_rh]
phica=[phicasf_lh, phicasf_rh]
phiam=[phiamsf_lh, phiamsf_rh]


markers='o'


### Io emisions : CML vs PhiIo

plt.figure()
plt.suptitle('Occurences structures fines - '+month+'/2021 - NDA/JunoN', fontsize='x-large')
plt.subplot(231)
cartes.io(cmlsf_lh,phiosf_lh, 'LH', markers)
plt.subplot(234)
cartes.io(cmlsf_rh,phiosf_rh, 'RH', markers)

#### Non-Io emisions

# CML vs PhiGa

plt.subplot(232)
cartes.gan(cmlsf_lh, phigasf_lh,cmlsf_lh,phiosf_lh,[342,51,134,173],[230,260,80,94],'LH', markers )
plt.subplot(235)
cartes.gan(cmlsf_rh, phigasf_rh,cmlsf_rh,phiosf_rh,[342,51,134,173],[230,260,80,94], 'RH', markers )
# plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9, hspace=0.3, wspace=0.1)
# plt.show()

# # CML vs PhiEu
#
#plt.figure()
#plt.suptitle('Occurences structures fines - '+month+'/2021 - NDA/JunoN', fontsize='x-large')
plt.subplot(233)
cartes.europa(cmlsf_lh, phieusf_lh, cmlsf_lh,phiosf_lh, cmlsf_lh, phigasf_lh,[342,51,134,173],[230,260,80,94],[125,206,312,35,145,184],[75,144,235,325,184,204], 'LH', markers )
plt.subplot(236)
cartes.europa(cmlsf_rh, phieusf_rh, cmlsf_rh,phiosf_rh, cmlsf_rh, phigasf_rh,[342,51,134,173],[230,260,80,94],[125,206,312,35,145,184],[75,144,235,325,184,204], 'RH', markers )
plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9, hspace=0.3, wspace=0.5)
plt.show()
#
# # CML vs PhiCa
#
# plt.figure()
# cartes.callisto(cml, phiam,'NDA/JunoN - '+month+'/2021', legends, markers )
# plt.show()
#
# # CML vs PhiAm
#
# plt.figure()
# cartes.amalthea(cml, phiam,'NDA/JunoN - '+month+'/2021', legends, markers )
# plt.show()